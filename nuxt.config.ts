// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  nitro: {
    esbuild: {
      options: {
        target: "esnext",
      },
    },
  },
  vite: {
    optimizeDeps: {
      esbuildOptions: {
        target: "esnext",
      },
    },
  },
  alias: {
    "@/*": `./*`,
  },

  devtools: { enabled: true },
  pages: true,

  app: {
    head: {
      viewport:
        "width=device-width, initial-scale=1, minimum-scale=0.5, maximum-scale=3.0 user-scalable=no",
      charset: "utf-8",
      htmlAttrs: {
        lang: "en",
      },
    },
  },

  components: [
    {
      path: "~/components",
      pathPrefix: false,
    },
  ],

  css: [
    // SCSS file in the project
    "@/assets/scss/main.scss",
    "@/assets/scss/_colors.scss",
    "@/assets/scss/_fonts.scss",
    "@/assets/scss/_media_when_min_width.scss",
  ],

  modules: ["@nuxt/image", ["@nuxtjs/robots", { UserAgent: "*", Allow: "/", BlankLine: true, Comment: 'Robot txt' }]],
  image: {
    // The screen sizes predefined by `@nuxt/image`:
    screens: {
      xs: 320,
      sm: 544,
      md: 768,
      lg: 960,
      mdl: 1080,
      xl: 1280,
      xxl: 1440,
    },
  },
  // image: {
  //   provider: "netlify",
  //   providers: {
  //     customProvider: {
  //       name: "assetsProvider",
  //       provider: "~/providers/assets-provider.ts", // must be created
  //       options: {
  //         baseURL: "/_nuxt/assets/img",
  //       },
  //     },
  //   },
  // },
});
