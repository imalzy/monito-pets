// import pet_1 from "~/assets/img/products/pet-1.jpg";
// import pet_2 from "~/assets/img/products/pet-2.jpg";
// import pet_3 from "~/assets/img/products/pet-3.jpg";
// import pet_4 from "~/assets/img/products/pet-4.jpg";
// import pet_5 from "~/assets/img/products/pet-5.jpg";
// import pet_6 from "~/assets/img/products/pet-6.jpg";
// import pet_7 from "~/assets/img/products/pet-7.jpg";
// import pet_8 from "~/assets/img/products/pet-7.jpg";

// import product_1 from "~/assets/img/products/product-1.jpg";
// import product_2 from "~/assets/img/products/product-2.jpg";
// import product_3 from "~/assets/img/products/product-3.jpg";
// import product_4 from "~/assets/img/products/product-4.jpg";
// import product_5 from "~/assets/img/products/product-5.jpg";
// import product_6 from "~/assets/img/products/product-6.jpg";
// import product_7 from "~/assets/img/products/product-7.jpg";
// import product_8 from "~/assets/img/products/product-8.jpg";

// import seller_1 from "~/assets/img/products/partner-1.png";
// import seller_2 from "~/assets/img/products/partner-2.png";
// import seller_3 from "~/assets/img/products/partner-3.png";
// import seller_4 from "~/assets/img/products/partner-4.png";
// import seller_5 from "~/assets/img/products/partner-5.png";
// import seller_6 from "~/assets/img/products/partner-6.png";
// import seller_7 from "~/assets/img/products/partner-7.png";

// import blog_1 from "~/assets/img/products/blog-1.jpg";
// import blog_2 from "~/assets/img/products/blog-2.jpg";
// import blog_3 from "~/assets/img/products/blog-3.jpg";

export const Pets = [
  {
    id: 1,
    petId: "MO231",
    title: "Pomeranian White",
    value2: "02 Months",
    price: 6900000,
    value1: "Male",
    thumbnail: "pets/pet-1.jpg",
  },
  {
    id: 2,
    petId: "MO502",
    title: "Poodle Tiny Yellow",
    value2: "02 Months",
    price: 3900000,
    value1: "Female",
    thumbnail: "pets/pet-2.jpg",
  },
  {
    id: 3,
    petId: "MO102",
    title: "Poodle Tiny Sepia",
    value2: "02 Months",
    price: 8900000,
    value1: "Female",
    thumbnail: "pets/pet-3.jpg",
  },
  {
    id: 4,
    petId: "MO231",
    title: "Pembroke Corgi Cream",
    value2: "02 Months",
    price: 7900000,
    value1: "Male",
    thumbnail: "pets/pet-4.jpg"
  },
  {
    id: 5,
    petId: "MO502",
    title: "Pembroke Corgi Tricolor",
    value2: "02 Months",
    price: 9000000,
    value1: "Male",
    thumbnail: "pets/pet-5.jpg"
  },
  {
    id: 6,
    petId: "MO231",
    title: "Pomeranian Whitee 2",
    value2: "02 Months",
    price: 8900000,
    value1: "Female",
    thumbnail: "pets/pet-6.jpg"
  },
  {
    id: 7,
    petId: "MO232",
    title: "Pomeranian Whitee",
    value2: "02 Months",
    price: 8900000,
    value1: "Female",
    thumbnail: "pets/pet-7.jpg"
  },
  {
    id: 8,
    petId: "MO512",
    title: "Poodle Tiny Dairy Cow",
    value2: "02 Months",
    price: 5000000,
    value1: "Male",
    thumbnail: "pets/pet-8.jpg"
  },
];

export const Products = [
  {
    id: 1,
    title: "Reflex Plus Adult Cat Food Salmon",
    value2: "385gm",
    price: 140000,
    value1: "Male",
    thumbnail: "products/product-1.jpg"
  },
  {
    id: 2,
    title: "Reflex Plus Adult Cat Food Salmon",
    value2: "1.5kg",
    price: 165000,
    value1: "Cat Food",
    thumbnail: "products/product-2.jpg"
  },
  {
    id: 3,
    title: "Cat scratching ball toy kitten sisal rope ball",
    value2: "",
    price: 1100000,
    value1: "Toy",
    thumbnail: "products/product-3.jpg"
  },
  {
    id: 4,
    title: "Cute Pet Cat Warm Nest",
    value2: "",
    price: 410000,
    value1: "Toy",
    thumbnail: "products/product-4.jpg"
  },
  {
    id: 5,
    title: "NaturVet Dogs - Omega - Gold Plus Salman Oil",
    value2: "385gm",
    price: 350000,
    value1: "Dog Food",
    thumbnail: "products/product-5.jpg"
  },
  {
    id: 6,
    title: "Costumes Fashion Pet Clother Cowboy Rider",
    value2: "1.5kg",
    price: 500000,
    value1: "Costume",
    thumbnail: "products/product-6.jpg"
  },
  {
    id: 7,
    title: "Costumes Chicken Drumsti ck Headband",
    value2: "",
    price: 400000,
    value1: "Costume",
    thumbnail: "products/product-7.jpg"
  },
  {
    id: 8,
    title: "Plush Pet Toy",
    value2: "",
    price: 5000000,
    value1: "Toy",
    thumbnail: "products/product-8.jpg"
  },
];

export const Sellers = [
  {
    id: 1,
    name: "sheba",
    thumbnail: "partners/partner-1.png",
  },
  {
    id: 2,
    name: "whiskas",
    thumbnail: "partners/partner-2.png",
  },
  {
    id: 3,
    name: "bokers",
    thumbnail: "partners/partner-3.png",
  },
  {
    id: 4,
    name: "felix",
    thumbnail: "partners/partner-4.png",
  },
  {
    id: 5,
    name: "Good Boy",
    thumbnail: "partners/partner-5.png",
  },
  {
    id: 6,
    name: "Butcher",
    thumbnail: "partners/partner-6.png",
  },
  {
    id: 7,
    name: "Petgree",
    thumbnail: "partners/partner-7.png",
  },
];

export const Blogs = [
  {
    id: 1,
    thumbnail: "blogs/blog-1.jpg",
    label: "Pet knowledge",
    title: "What is a Pomeranian? How to Identify Pomeranian Dogs",
    desc: "The Pomeranian, also known as the Pomeranian (Pom dog), is always in the top of the cutest pets. Not only that, the small, lovely, smart, friendly, and skillful circus dog breed.",
  },
  {
    id: 2,
    thumbnail: "blogs/blog-2.jpg",
    label: "Pet knowledge",
    title: "Dog Diet You Need To Know",
    desc: "Dividing a dog's diet may seem simple at first, but there are some rules you should know so that your dog can easily absorb the nutrients in the diet. For those who are just starting to raise dogs, especially newborn puppies with relatively weak resistance.",
  },
  {
    id: 3,
    thumbnail: "blogs/blog-3.jpg",
    label: "Pet knowledge",
    title:
      "Why Dogs Bite and Destroy Furniture and How to Prevent It Effectively",
    desc: "Dog bites are common during development. However, no one wants to see their furniture or important items being bitten by a dog.",
  },
];
